import * as React from 'react';
export declare type ConsoleProps = {
    consoleOutput: any;
    consoleInput: string;
    handleOnChangeForInput: React.FormEventHandler;
    handleSubmit: React.FormEventHandler;
    legendData: any;
};
/**
 * Console presentational component
 *
 * @param { ConsoleProps } props
 *
 * @since 0.1.0
 */
export declare const Console: React.SFC<ConsoleProps>;
/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
