import * as React from 'react';
export declare type ConsoleLegendItemProps = {
    legendKey: string;
    legendDescData: string;
    legendUsageData: string;
    handleMouseOver: React.FormEventHandler;
    handleMouseOut: React.FormEventHandler;
};
/**
 * ConsoleLegendItem presentational component
 *
 * @param { ConsoleLegendItemProps } props
 *
 * @since 0.1.0
 */
export declare const ConsoleLegendItem: React.SFC<ConsoleLegendItemProps>;
/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
