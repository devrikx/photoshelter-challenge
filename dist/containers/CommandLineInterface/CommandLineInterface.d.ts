import * as React from 'react';
export interface CommandLineInterfaceProps {
    consoleItems: string[];
}
/**
 * CommandLineInterfacecontainer component
 *
 * @since 0.1.0
 */
export declare class CommandLineInterface extends React.Component<CommandLineInterfaceProps, any> {
    /**
     * @var { string } displayName Always set the display name
     *
     * @since 0.1.0
     */
    displayName: string;
    /**
    * We'd set propTypes here if this wasn't Typescript.
    * We'd set defaultProps here if we wanted any.
    */
    /**
     * Constructor
     *
     * @param props
     *
     * @since 0.1.0
     */
    constructor(props?: CommandLineInterfaceProps);
    /**
     * Method to run when a component re-renders
     *
     * @param { void }
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    componentDidUpdate(): void;
    handleOnMouseEnterLegend(event: any): void;
    handleOnMouseLeaveLegend(event: any): void;
    /**
     * Method for handling when our virutal-terminal input value is changed:
     *
     * @param { React.FormEventHandler } event The React FormEventHandler
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    handleOnChangeForInput(event: any): void;
    /**
     * Method for handling when our virutal-terminal input form is submitted
     *
     * @param { React.FormEventHandler } event The React FormEventHandler
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    handleSubmit(event: any): void;
    /**
     * Method for putting together our console output for rendering:
     *
     * @param { void }
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    getConsoleOutput(): JSX.Element[];
    getConsoleLegendOutput(): JSX.Element[];
    /**
     * Container components pass rendering responsibilities off to
     * presentational components
     *
     * @param { void }
     *
     * @since 1.0.0
     */
    render(): JSX.Element;
}
declare const _default: import("react-redux").ConnectedComponentClass<typeof CommandLineInterface, Pick<CommandLineInterfaceProps, never>>;
export default _default;
