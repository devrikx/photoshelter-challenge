export declare type FileSystemMetaType = {
    type: string;
    link?: string | null;
};
export declare type FileSystemType = {
    [key: string]: FileSystemMetaType;
};
export declare type FileSystemErrorType = {
    error: string;
    message: string;
};
export declare class Filesystem {
    /**
     * @var { string } currentDirectory Stores the current working directory.
     */
    currentDirectory: string;
    /**
     * @var { FileSystemType } filesystem The current representation of a virtual filesystem
     */
    filesystem: FileSystemType;
    /**
     * Class constructor.
     *
     * @since 1.0.0
     */
    constructor(cwd?: string, filesystem?: FileSystemType);
    /**
     * Method which checks if a path identifier is valid
     *
     * @param { string } path An absolute or relative path string
     *
     * @return { boolean } True if a path string is valid, false if not
     */
    isValidIdentifier(path: string): boolean;
    /**
     * Method that checks if a path is absolute or relative, and subsequently
     * returns an absolute path
     *
     * @param { string } path An absolute or relative path
     *
     * @return { string } An absolute path
     *
     * @since 1.0.0
     */
    getFullPath(path: string): string;
    /**
     * A method which checks to see that a path exists within the filesystem. It will call
     * checkPathExistsViaSymlink first, then check if the path exists directly..
     *
     * @param { string } path The absolute path to the directory which needs to be checked for existence
     *
     * @return { number|boolean } 1 if the path exists directly, 2 if the path exists via symlink, or false if not. .
     */
    checkPathExists(path: string): number | boolean;
    /**
     * A method which checks to see that a target path exists within the filesystem through the use of symlinks
     *
     * @param { string } path The absolute path to the directory which needs to be checked for existence
     *
     * @return { number|boolean } 2 if the path exists via symlink, false if not.
     */
    checkPathExistsViaSymlink(path: string): number | boolean;
    /**
     * A method which finds a symlink within a path chain, if one exists
     *
     * @param { string } path The aboslute path to a target which possibly possess a symlink reference along the chain
     *
     * @return { string|boolean } If the path is found to contain a symlink, the symlink portion is returned - otherwise false is returned.
     *
     * @since 1.0.0
     */
    findSymlinkInPath(path: string): boolean | string;
    /**
     * Method which builds a real path by replacing a symlink portion of the path chain.
     *
     * @param { string } path A relative or absolute path to a target
     *
     * @return { string } The original path with a symlink portion of its chain replaced by the symlink's link, or REAL, path.
     *
     * @since 1.0.0
     */
    replaceSymlinkInPath(path: string): string;
    /**
     * A method which checks and returns the full/absolute REAL path of a target.
     *
     * @param { string } path The absolute or relative path to the directory.
     *
     * @return { string|boolean } If the path exists,the full/absolute REAL path string is returned, false if not.
     */
    getRealPath(path: string): string | false;
    /**
     * Method that returns the absolute path to the parent directory of a directory or file that is  passed in as an absolute path
     *
     * @param { string } path The absolute path to a file or directory
     *
     * @return { string } The aboslute path to the parent directory of the path that's provided
     */
    getParentDirectory(path: string): string;
    /**
     * Method which checks for children within a directory.
     *
     * @param { string } path The absolute path to a directory.
     * @param { boolean } check A boolean for whether to simply report a child is present, or to return a list of children.
     *
     * @return { boolean|Array<string> } A boolean for whether children exist or not, otherwise an array of children is returned if found.
     *
     * @since 1.0.0
     */
    getChildren(path: string, check?: boolean): boolean | Array<string>;
    /**
     * Method which checks for direct children of a directory.
     *
     * @param { string } path The absolute path to a directory.
     *
     * @return { boolean|Array<any> } An array of children is returned if found, otherwise false is returned.
     *
     * @since 1.0.0
     */
    getDirectChildren(path: string): boolean | any;
    /**
     * Method to change directories.
     *
     * @param { string } path A relative or absolute path to another directory. Can use `..` to signify the current directory's parent directory.
     *
     * @return { boolean|FileSystemErrorType } True if the directory was changed, the error for why-not if it was not.
     *
     * @since 1.0.0
     */
    cd(path: string): boolean | FileSystemErrorType;
    /**
     * Method to print the path to the current working directory.
     *
     * @param { void }
     *
     * @return { string } The absolute path to the current working directory.
     *
     * @since 1.0.0
     */
    pwd(): string;
    /**
     * A method to add a new directory to the filesystem.
     *
     * @param { string } path The absolute or relative path to the new directory to create.
     *
     * @return { boolean|FileSystemErrorType } True; signifying success, or an error message explaining why the new directory could not be made.
     *
     * @since 1.0.0
     */
    mkdir(path: string): boolean | FileSystemErrorType;
    /**
     * A method to remove a directory from the filesystem.
     *
     * @param { string } path The absolute or relative path to the directory to remove.
     *
     * @return { boolean|FileSystemErrorType } True; signifying success, or an error message explaining why the directory could not be removed.
     *
     * @since 1.0.0
     */
    rmdir(path: string): boolean | FileSystemErrorType;
    /**
     * A method to register a symlink within the filesystem.
     *
     * @param { string } source The absolute or relative path to the directory or file within the filesystem that the symlink being created should point to.
     * @param { string } dest The absolute or relative path to where a symlink directory or file should be created within the filesystem.
     *
     * @return { boolean|FileSystemErrorType } True; signifying success, or an error message explaining why the symlink could not be created.
     *
     * @since 1.0.0
     */
    symlink(source: string, dest: string): boolean | FileSystemErrorType;
    /**
     * A method to create a new file within the filesystem
     *
     * @param { string } path The absolute or relative path leading to and including the file name, where a file should be created within the filesystem.
     *
     * @return { boolean|FileSystemErrorType } True; signifying success, or an error message explaining why the file could not be created.
     *
     * @since 1.0.0
     */
    touch(path: string): boolean | FileSystemErrorType;
}
