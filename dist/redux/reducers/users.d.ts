declare const users: (state: {
    loading: boolean;
    error: any;
    users: {
        invalidate: boolean;
        lastUpdated: any;
        list: any;
    };
}, action: any) => {
    loading: boolean;
    error: any;
    users: {
        invalidate: boolean;
        lastUpdated: any;
        list: any;
    };
};
export default users;
