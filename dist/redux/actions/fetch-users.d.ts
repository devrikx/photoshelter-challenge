export declare const fetchUsersBegin: () => {
    type: string;
};
export declare const fetchUsersSuccess: (users: any) => {
    type: string;
    payload: {
        users: any;
    };
};
export declare const fetchUsersError: (error: any) => {
    type: string;
    payload: {
        error: any;
    };
};
/**
 * Method to get a list of users from our API via `fetch`
 *
 * @param { void }
 *
 * @return { Promise<any> }
 *
 * @since 1.0.0
 */
export declare function fetchUsers(): (dispatch: any) => Promise<any>;
