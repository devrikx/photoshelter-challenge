/*-----------------------------------------------------------------------------
 * @package:    kwaeri-core-react
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    1.0.0
 *---------------------------------------------------------------------------*/


// INCLUDES


// DEFINES
export type ParsedInputType = {
    command: boolean|string;
    arguments: boolean|Array<string>;
    error?: string;
}


export class FilesystemHelper
{
    commands =
    {
        cd: {
            desc: "Changes the current directory.",
            usage: "cd <path>"
        },
        pwd: {
            desc: "Prints the current working directory.",
            usage: "pwd"
        },
        mkdir: {
            desc: "Creates a new directory.",
            usage: "mkdir <path>"
        },
        rmdir: {
            desc: "Deletes a directory.",
            usage: "rmdir <path>"
        },
        symlink: {
            desc: "Creates a symbolic link to a directory or file.",
            usage: "symlink <linked> <link>"
        },
        touch: {
            desc: "Creates an empty file.",
            usage: "touch <path>"
        },
        ls: {
            desc: "Diplays the contents of either the current directory, or of a specified directory.",
            usage: "ls (<path>)"
        }
    }

    /**
     * Class constructor
     *
     * @since 0.1.0
     */
    constructor()
    {
    }


    /**
     * Method which parses input in order to determine a command and provided arguments
     *
     * @param { string } input The string input provided by the user of a virtual terminal
     *
     * @return { ParsedInputType } An object which specifies the found command and arguments, or an error otherwise
     *
     * @since 1.0.0
     */
    parseInput( input: string ): ParsedInputType
    {
        // Right away let's parse input and see what we find:
        let parsed: Array<string> = input.match( /(?:^|)([a-zA-Z\/\.]+)(?:$|)/gm );

        // We are only interested in the first 2 - 3 matches at most. Additionally, the first
        // match must be found within our commands array in order to be valid:
        if( parsed && parsed.length > 0 && Object.keys( this.commands ).indexOf( parsed[0] ) >= 0 )
        {
            // Great! Let's go ahead and return the appropriate object structure:
            return { command: parsed[0], arguments: parsed.splice( 1, parsed.length ) };
        }

        // Let's figure out the error and return that for the user to contemplate:
        let error = "";

        // Did parse not match anything?
        if( !parsed || !parsed.length )
        {
            error += "Command not found.";
        }

        // Was the command provided not an allowed command?
        if( !( parsed[0] in this.commands ) )
        {
            error += `The command '${parsed[0]}' is not valid.`;
        }

        // Let the end user know what hit:
        return { command: false, arguments: false, error: error };
    }

    /**
     * Runs a routine which is described in exercise 1 of the Photoshelter challenge
     *
     * @return { Array<string> } Returns an array of strings to be potentially displayed in a virtual console/terminal.
     */
    runExercise1(): string
    {
        return "";
    }
}

