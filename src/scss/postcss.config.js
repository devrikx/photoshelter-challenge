/*-----------------------------------------------------------------------------
 * @package:    kwaeri-core-react
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


module.exports =
{
    plugins: [require("autoprefixer")]
};

