/*------------------------------------------------------------------------------
 * @package:   kwaeri-core-react
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import {
    ADD_CONSOLE_ITEM
} from '../constants/action-types';


// DEFINES


/* An action for adding console items */
export const addConsoleItem = ( item: any ) => ( { type: ADD_CONSOLE_ITEM, payload: item } );
