/*------------------------------------------------------------------------------
 * @package:   kwaeri-core-react
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import fetch from 'cross-fetch';
import {
    FETCH_USERS_BEGIN,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_ERROR,
    ADD_CONSOLE_ITEM
} from '../constants/action-types';


// DEFINES



/* Dispatch Actions for fetching a items from an API: */
export const fetchUsersBegin = () => ( { type: FETCH_USERS_BEGIN } );

export const fetchUsersSuccess = ( users: any ) => ( { type: FETCH_USERS_SUCCESS, payload: { users } } );

export const fetchUsersError = ( error: any ) => ( { type: FETCH_USERS_ERROR, payload: { error } } );


/**
 * Method to get a list of users from our API via `fetch`
 *
 * @param { void }
 *
 * @return { Promise<any> }
 *
 * @since 1.0.0
 */
export function fetchUsers()
{
    console.log( 'Fetching users!' );
    return ( dispatch: any ) => {

        dispatch( fetchUsersBegin() );
        return fetch( "https://www.snagfilms.com/apis/films.json?limit=10" )
        .then( handleErrors )
        .then( response => response.json() )
        .then
        (
            ( json ) =>
            {
                console.log( 'Dispatching success action: ' );
                console.log( json.users );

                dispatch( fetchUsersSuccess( json.users ) );

                return json.users;
            }
        )
        .catch( ( error ) => dispatch( fetchUsersError( error ) ) );
    };
}


/**
 * A method to handle HTTP Errors
 *
 * @param { any } response The fetch response
 *
 * @return { any } Returns the response if no errors, else it throws the error
 *
 * @since 1.0.0
 */
function handleErrors( response: any )
{
    if( !response.ok )
    {
        throw Error( response.statusText );
    }

    return response;
}
