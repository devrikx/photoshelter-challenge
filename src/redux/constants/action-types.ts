/*------------------------------------------------------------------------------
 * @package:   kwaeri-core-react
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES


// DEFINES


// CONSTANTS

/* The loading state, an action for displaying a loader and wiping errors: */
export const FETCH_USERS_BEGIN = "FETCH_USERS_BEGIN";

/* The success state, an action for displaying API items: */
export const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";

/* The failure state, an action for displaying errors: */
export const FETCH_USERS_ERROR = "FETCH_USERS_ERROR";

/* Constants for working with console items that will be displayed */
export const ADD_CONSOLE_ITEM = "ADD_CONSOLE_ITEM";
