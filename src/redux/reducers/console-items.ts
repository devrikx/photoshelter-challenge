/*------------------------------------------------------------------------------
 * @package:   kwaeri-core-react
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import {
    ADD_CONSOLE_ITEM
} from '../constants/action-types';


// DEFINES
const initialState =
{
    consoleItems: <any>[]
};


const consoleItems = ( state = initialState, action: any ) =>
{
    console.log( 'Action.type = ' )
    console.log( action.type );
    switch( action.type )
    {
        case ADD_CONSOLE_ITEM:
        {
            // Return a new state object that is a copy of the original state
            // plus the additions present in the payload:
            return { ...state, consoleItems: [...state.consoleItems, action.payload] };
        }break;

        default:
        {
            return state;
        }break;
    }
};


// Export it:
export default consoleItems;