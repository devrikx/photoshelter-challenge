/*------------------------------------------------------------------------------
 * @package:   kwaeri-core-react
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { combineReducers } from 'redux';
import users from './users';
import consoleItems from './console-items';


// DEFINES
const rootReducer = combineReducers({
    users,
    consoleItems
});


// Export it:
export default rootReducer;
