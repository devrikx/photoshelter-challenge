/*------------------------------------------------------------------------------
 * @package:   kwaeri-core-react
 * @author:    Richard B Winters
 * @copyright: 2018 Massively Modified, Inc..
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { UnderConstructionProps, UnderConstruction } from './';


// Basic ErrorHandler Test:
it
(
    'ErrorHandler renders correctly',
    () =>
    {
        let errorHandlerProperties: UnderConstructionProps =
        {
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <UnderConstruction/>
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
