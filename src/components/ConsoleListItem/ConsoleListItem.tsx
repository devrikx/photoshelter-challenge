/*-----------------------------------------------------------------------------
 * @package:    ConsoleListItem
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import { styles } from './';
import * as React from 'react';


// DEFINES
export type ConsoleListItemProps = {
    content: string;
};


/**
 * ConsoleListItem presentational component
 *
 * @param { ConsoleListItemProps } props
 *
 * @since 0.1.0
 */
export const ConsoleListItem: React.SFC<ConsoleListItemProps> = ( props: any ) =>
{
    // Here we render the component:
    return(
        <li className={styles.consoleListItem}>
            {props.content}
        </li>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
ConsoleListItem.displayName = 'ConsoleListItem';


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */

