/*-----------------------------------------------------------------------------
 * @package:    ConsoleListItem
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { ConsoleListItemProps, ConsoleListItem } from './';


// Basic ConsoleListItem Test:
it
(
    'ConsoleListItem renders correctly',
    () =>
    {
        let componentProperties: ConsoleListItemProps =
        {
            content: "A simple test string for the ConsoleListItem component test."
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <ConsoleListItem
              content={componentProperties.content}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);

