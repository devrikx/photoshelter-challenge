/*------------------------------------------------------------------------------
 * @package:   kwaeri-core-react
 * @author:    Richard B Winters
 * @copyright: 2018 Massively Modified, Inc..
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from './';
import * as React from 'react';
import { Switch, Route, HashRouter } from 'react-router-dom';
import CommandLineInterface from '../../containers/CommandLineInterface';
import { UnderConstruction } from '../UnderConstruction';
import { ErrorHandler } from '../ErrorHandler';


// DEFINES
export interface KwaeriCoreProps { title?: string; }


/**
 * KwaeriCore component
 *
 * @param { KwaeriCoreProps } props
 *
 * @since 0.1.0
 */
export const KwaeriCore: React.SFC<KwaeriCoreProps> = ( props ) =>
{
    // Here we render the component:
    return (
        <HashRouter>
            <div className={styles.kwaeriAppContainerInner}>
                <div className={"row " + styles.contentBlock}>
                    <div className={"col-md-12 "  + styles.contentBlockInner}>
                        <Switch>
                            <Route exact path="/" component={CommandLineInterface}/>
                            <Route component={ErrorHandler} />
                        </Switch>
                    </div>
                </div>
            </div>
        </HashRouter>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
KwaeriCore.displayName = "Kwaeri Core";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted to.
 */
