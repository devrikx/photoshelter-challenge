/*-----------------------------------------------------------------------------
 * @package:    ConsoleList
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import { styles } from './';
import * as React from 'react';
import { ConsoleListItem } from '../ConsoleListItem';


// DEFINES
export type ConsoleListProps = {
    consoleItemList: ConsoleListItem[]
};


/**
 * ConsoleList presentational component
 *
 * @param { ConsoleListProps } props
 *
 * @since 0.1.0
 */
export const ConsoleList: React.SFC<ConsoleListProps> = ( props: any ) =>
{


    // Here we render the component:
    return(
        <div className={styles.consoleContentInner}>
            <ul className={styles.consoleList}>
                {props.consoleItemList}
            </ul>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
ConsoleList.displayName = 'ConsoleList';


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */

