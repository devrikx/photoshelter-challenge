/*-----------------------------------------------------------------------------
 * @package:    ConsoleLegendItem
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import { styles } from './';
import * as React from 'react';


// DEFINES
export type ConsoleLegendItemProps = {
    legendKey: string;
    legendDescData: string;
    legendUsageData: string;
    handleMouseOver: React.FormEventHandler;
    handleMouseOut: React.FormEventHandler;
};


/**
 * ConsoleLegendItem presentational component
 *
 * @param { ConsoleLegendItemProps } props
 *
 * @since 0.1.0
 */
export const ConsoleLegendItem: React.SFC<ConsoleLegendItemProps> = ( props ) =>
{
    // Here we render the component:
    return(
        <li className={styles.consoleLegendItem}>
            <span
              onMouseOver={props.handleMouseOver}
              onMouseOut={props.handleMouseOut}
              className={styles.legendKey}
              id={props.legendKey}>{props.legendKey}</span>
            <div className={styles.legendData + " legend-hidden"} id={props.legendKey + "-data"}>
                <span
                  className={styles.legendDescriptionTitle}>Description</span>
                <span
                  className={styles.legendDataDescription}>{props.legendDescData}</span>
                <span
                  className={styles.legendUsageTitle}>Usage</span>
                <span
                  className={styles.legendDataUsage}>{props.legendUsageData}</span>
            </div>
        </li>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
ConsoleLegendItem.displayName = 'ConsoleLegendItem';


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */

