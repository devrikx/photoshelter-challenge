export const consoleLegendItem: string;
export const legendKey: string;
export const legendData: string;
export const legendDescriptionTitle: string;
export const legendDataDescription: string;
export const legendUsageTitle: string;
export const legendDataUsage: string;
