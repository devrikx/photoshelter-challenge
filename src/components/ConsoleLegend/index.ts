/*-----------------------------------------------------------------------------
 * @package:    ConsoleLegend
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as styles from './styles.scss';


// EXPORTS
export { styles };
export {
    ConsoleLegend,
    ConsoleLegendProps
} from './ConsoleLegend';

