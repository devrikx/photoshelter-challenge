/*-----------------------------------------------------------------------------
 * @package:    ConsoleLegend
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import { styles } from './';
import * as React from 'react';


// DEFINES
export type ConsoleLegendProps = {
    content: any;
};


/**
 * ConsoleLegend presentational component
 *
 * @param { ConsoleLegendProps } props
 *
 * @since 0.1.0
 */
export const ConsoleLegend: React.SFC<ConsoleLegendProps> = ( props ) =>
{
    // Here we render the component:
    return(
        <div className={styles.consoleLegendContainer}>
            <div className={styles.consoleLegendContainerInner}>
                <strong>Legend</strong>
                <ul className={styles.consoleLegendList}>
                    {props.content}
                </ul>
            </div>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
ConsoleLegend.displayName = 'ConsoleLegend';


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */

