export const consoleContainer: string;
export const consoleContainerWindowTitle: string;
export const consoleContainerInner: string;
export const consoleContent: string;
export const consoleInput: string;
export const consoleInputPrefix: string;
export const consoleInputInput: string;
