/*-----------------------------------------------------------------------------
 * @package:    Console
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import { styles } from './';
import * as React from 'react';
import { ConsoleList } from '../ConsoleList';
import { ConsoleLegend } from '../ConsoleLegend';


// DEFINES
export type ConsoleProps = {
    consoleOutput: any;
    consoleInput: string;
    handleOnChangeForInput: React.FormEventHandler;
    handleSubmit: React.FormEventHandler;
    legendData: any;
};


/**
 * Console presentational component
 *
 * @param { ConsoleProps } props
 *
 * @since 0.1.0
 */
export const Console: React.SFC<ConsoleProps> = ( props: any ) =>
{
    // Here we render the component:
    return(
        <div className={styles.consoleContainer}>
            <h6 className={styles.consoleContainerWindowTitle}>Virtual Terminal</h6>
            <div className={styles.consoleContainerInner}>
                <div id="consoleOutput" className={styles.consoleContent + " console-font"}>
                    <ConsoleList
                      consoleItemList={props.consoleOutput}
                    />
                </div>
                <div className={styles.consoleInput}>
                    <form autoComplete="off" onSubmit={props.handleSubmit}>
                        <span className={styles.consoleInputPrefix}>$</span>
                        <input
                          id="consoleInput"
                          name="consoleInput"
                          className={styles.consoleInputInput + " console-font"}
                          onChange={props.handleOnChangeForInput}
                          value={props.consoleInput}
                          autoFocus />
                    </form>
                </div>
            </div>
            <ConsoleLegend
              content={props.legendData}
            />
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
Console.displayName = 'Console';


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */

