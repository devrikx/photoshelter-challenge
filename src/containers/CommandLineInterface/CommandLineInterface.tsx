/*-----------------------------------------------------------------------------
 * @package:    CommandLineInterface
 * @author:     Richard B Winters
 * @copyright:  2018 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import { connect } from 'react-redux';
import { addConsoleItem } from '../../redux/actions';
import { Console } from '../../components/Console';
import { ConsoleListItem } from '../../components/ConsoleListItem';
import { ConsoleLegendItem } from '../../components/ConsoleLegendItem';
import { FilesystemHelper, ParsedInputType } from '../../helpers';
import { Filesystem } from '../../library';


// DEFINES
export interface CommandLineInterfaceProps {
    consoleItems: string[]; }

interface StateFromProps {
    consoleItems: string[];
}

const mapStateToProps = ( state: any ) => ({
    consoleItems: state.consoleItems.consoleItems
});

interface DispatchFromProps {
    addConsoleItem: ( item: any ) => any
}

const mapDispatchToProps = ( dispatch: any ) =>
({
    addConsoleItem: ( item: any ) => dispatch( addConsoleItem( item ) )
});

/* Instantiating the Filesystem Library */
let filesystemHelper = new FilesystemHelper();


/**
 * CommandLineInterfacecontainer component
 *
 * @since 0.1.0
 */
export class CommandLineInterface extends React.Component<CommandLineInterfaceProps, any>
{
    /**
     * @var { string } displayName Always set the display name
     *
     * @since 0.1.0
     */
    displayName = 'CommandLineInterface';


    /**
    * We'd set propTypes here if this wasn't Typescript.
    * We'd set defaultProps here if we wanted any.
    */


    /**
     * Constructor
     *
     * @param props
     *
     * @since 0.1.0
     */
    constructor( props?: CommandLineInterfaceProps )
    {
        super( props );

        // Define the shape of our state:
        this.state = {

            // Our filesystem is an object. An object is ideal for representing
            // the filesystem.
            //
            // All directories and files under root will be represented within
            // a property of the filesystem object. Each property key is a full
            // path to either a directory or file. Each property is an object
            // with at least 1 property: type - which specifies whether the
            // item is a directory or file.
            //
            // For symlinks, the full path to the linked directory or file can
            // be specified in a 'link' property. The key will still be a path
            // with a type property specified within its object association
            // of file or directory.
            //
            // There is no nesting, the map is flat, and logic checks that logical
            // paths are present in order to allow for new files or directories to
            // be created..
            //
            // The limitations for the directory and file names allow us to ensure
            // property names will work to represent the respective names directly.
            fs: new Filesystem(),

            // The current directory is stored as a string which is the full path
            // from root. We can 'split' this string by '/' to determine how to
            // virtually navigate - allowing us to validate paths, as well as to
            // determine whether directories and/or files can be created or removed.
            currentDirectory: "/",

            // The current console input:
            consoleInput: "",

            // The current console output is mapped via Redux.

            // Tracks legend hover status:
            legendHover: false
        };

        // Bind methods/handlers so we can pass them for invocation elsewhere
        // while keeping them bound to 'this' instance:
        this.getConsoleOutput = this.getConsoleOutput.bind( this );
        this.getConsoleLegendOutput = this.getConsoleLegendOutput.bind( this );
        this.handleOnChangeForInput = this.handleOnChangeForInput.bind( this );
        this.handleSubmit = this.handleSubmit.bind( this );
        this.handleOnMouseEnterLegend = this.handleOnMouseEnterLegend.bind( this );
        this.handleOnMouseLeaveLegend = this.handleOnMouseLeaveLegend.bind( this );
    }


    /**
     * Method to run when a component re-renders
     *
     * @param { void }
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    componentDidUpdate()
    {
        // Let's get a handle to the consoleOutput div:
        let consoleOutput = document.getElementById( 'consoleOutput' );

        // And ensure that it's always scrolled to the bottom:
        consoleOutput.scrollBy( 0, consoleOutput.scrollHeight );
    }


    handleOnMouseEnterLegend( event: any )
    {
        let dataDiv = document.getElementById( event.target.id + "-data" );

        dataDiv.classList.remove( "legend-hidden" );
    }


    handleOnMouseLeaveLegend( event: any )
    {
        let dataDiv = document.getElementById( event.target.id + "-data" );

        dataDiv.classList.add( "legend-hidden" );
    }


    /**
     * Method for handling when our virutal-terminal input value is changed:
     *
     * @param { React.FormEventHandler } event The React FormEventHandler
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    handleOnChangeForInput( event: any )
    {
        // Here we handle validating input for the console...?
        let currentInput = event.target.value;

        // Do something with currentInput...

        // Update the consoleInput in our state:
        this.setState( { [event.target.id]: event.target.value } );
    }


    /**
     * Method for handling when our virutal-terminal input form is submitted
     *
     * @param { React.FormEventHandler } event The React FormEventHandler
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    handleSubmit( event: any )
    {
        // Prevent the default from taking place:
        event.preventDefault();

        // Destructure our state object temporarily:
        let { consoleInput, fs } = this.state;

        // Create a prefix for consoleInput display:
        let consoleItemPrefix = "virtual:" + fs.currentDirectory + "$ ";

        // Add consoleItemPrefix + consoleInput as a consoleItem:
        ( this.props as any ).addConsoleItem( <span className="input-item">{consoleItemPrefix + consoleInput}</span> );

        // Next, parse the user's input:
        let parsedInput: any = filesystemHelper.parseInput( consoleInput );

        // Some test output:

        // Prepare an output container:
        let output: any = null;

        // Process the parsed input from the user:
        switch( parsedInput.command )
        {
            case 'cd':
            {
                if( parsedInput.arguments )
                {
                    output = fs.cd( parsedInput.arguments[0] );
                }
            }break;

            case 'mkdir':
            {
                if( parsedInput.arguments )
                {
                    output = fs.mkdir( parsedInput.arguments[0] );
                }
            }break;

            case 'rmdir':
            {
                if( parsedInput.arguments )
                {
                    output = fs.rmdir( parsedInput.arguments[0] );
                }
            }break;

            case 'pwd':
            {
                output = fs.pwd();

                // This is supposed to print out information to the console output:
                ( this.props as any ).addConsoleItem( output );
            }break;

            case 'symlink':
            {
                if( parsedInput.arguments )
                {
                    output = fs.symlink( parsedInput.arguments[0], parsedInput.arguments[1] );
                }
            }break;

            case 'touch':
            {
                if( parsedInput.arguments )
                {
                    output = fs.touch( parsedInput.arguments[0] );
                }
            }break;

            case 'ls':
            {
                // Prepare input for the method:
                let supplied = ( parsedInput && parsedInput.arguments && parsedInput.arguments[0] ) ? parsedInput.arguments[0] : fs.currentDirectory;

                // Get a list of directories or files:
                output = fs.getDirectChildren( supplied );

                // Prepare content using tsx:
                if( output && Object.keys( output ).length > 0 && !output.hasOwnProperty( "error" ) )
                {
                    let content = Object.keys( output ).map(
                        ( key: any, index: any ) => (
                            <span className={output[key].type + "-item " + ( ( output[key].link ) ? "symlink-item" : "" )}>{key}</span>
                        )
                    );

                    // This is supposed to print out information to the console output:
                    ( this.props as any ).addConsoleItem( content );
                }

                // If no children are found no consoleItem should be created
            }break;

            default:
            {
                output = <span className="error-item">{parsedInput.error}</span>;

                // This is supposed to print out information to the console output:
                ( this.props as any ).addConsoleItem( output );
            }
        }

        // Spew out any erros that need to be spewed out:
        if( output && typeof output == "object" && output.hasOwnProperty( "error" ) )
        {
            // This is supposed to print out information to the console output:
            ( this.props as any ).addConsoleItem( <span className="error-item">{output.error + " " + output.message}</span> );
        }

        // Clear the state of the input for a subsequent run-through:
        this.setState( { consoleInput: "", fs: fs } );
    }


    /**
     * Method for putting together our console output for rendering:
     *
     * @param { void }
     *
     * @return { void }
     *
     * @since 1.0.0
     */
    getConsoleOutput()
    {
        // This method will render the console-output-list component, populated
        // with console-output-list-items using the data stored in our state.
        const { consoleItems } = this.props;

        return (
            consoleItems.map(
                ( item: any ) => (
                    <ConsoleListItem
                      content={item}
                    />
                )
            )
        );

    }


    getConsoleLegendOutput()
    {
        return (
            Object.keys( filesystemHelper.commands ).map(
                ( item: any, index: any ) => (
                    <ConsoleLegendItem
                      legendKey={item}
                      legendDescData={(filesystemHelper as any).commands[item].desc}
                      legendUsageData={(filesystemHelper as any).commands[item].usage}
                      handleMouseOver={this.handleOnMouseEnterLegend}
                      handleMouseOut={this.handleOnMouseLeaveLegend}
                    />
                )
            )
        );
    }


    /**
     * Container components pass rendering responsibilities off to
     * presentational components
     *
     * @param { void }
     *
     * @since 1.0.0
     */
    render()
    {
        const { consoleInput } = this.state;

        return (
            <Console
              consoleOutput={this.getConsoleOutput()}
              consoleInput={consoleInput}
              handleOnChangeForInput={this.handleOnChangeForInput}
              handleSubmit={this.handleSubmit}
              legendData={this.getConsoleLegendOutput()}
            />
        );
    }
}


// Here we wrap the CommandLineInterface Container with a Connect container,
// creating the CommandLineInterfaceContainer:
export default connect<StateFromProps, DispatchFromProps>(
    mapStateToProps,
    mapDispatchToProps
)( CommandLineInterface );

