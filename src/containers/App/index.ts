/*------------------------------------------------------------------------------
 * @package:   kwaeri-react-app
 * @author:    Richard B Winters
 * @copyright: 2018 Massively Modified, Inc..
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as styles from './styles.scss';


// EXPORTS
export { styles };
export {
    App,
    AppProps
} from './App';
